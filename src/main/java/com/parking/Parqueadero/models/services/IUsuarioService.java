package com.parking.Parqueadero.models.services;

import com.parking.Parqueadero.models.entity.Parqueadero;
import com.parking.Parqueadero.models.entity.Usuario;
import com.parking.Parqueadero.models.entity.Vehiculo;

import java.util.List;

public interface IUsuarioService {
    List<Vehiculo> calluser();
    List<Parqueadero> findAllPark();

    void guardarusuario(Usuario usuario, Vehiculo vehiculo);

    void saveparqueadero(Parqueadero parqueadero);

    Parqueadero encontrarparqueadero(Long id);

    Vehiculo encontrarvehiculo(String placa);

    Long contarvehiculospark();

    void eliminarparq(Long id);
}


