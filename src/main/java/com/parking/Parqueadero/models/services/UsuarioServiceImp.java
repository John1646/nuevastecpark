package com.parking.Parqueadero.models.services;

import com.parking.Parqueadero.models.dao.ParqueaderoDao;
import com.parking.Parqueadero.models.dao.UsuarioDao;
import com.parking.Parqueadero.models.dao.VehiculoDao;
import com.parking.Parqueadero.models.entity.Parqueadero;
import com.parking.Parqueadero.models.entity.Usuario;
import com.parking.Parqueadero.models.entity.Vehiculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UsuarioServiceImp implements IUsuarioService{
    @Autowired
    UsuarioDao User_dao;
    @Autowired
    VehiculoDao Vehi_dao;
    @Autowired
    ParqueaderoDao parqu_dao;
    @Override
    public List<Vehiculo> calluser() {
        return Vehi_dao.findAll();
    }

    @Override
    public List<Parqueadero> findAllPark() {
        return parqu_dao.findAll();
    }

    @Override
    public void guardarusuario(Usuario usuario, Vehiculo vehiculo) {
        User_dao.save(usuario);
        vehiculo.setUsuario(usuario);
        Vehi_dao.save(vehiculo);
    }

    @Override
    public void saveparqueadero(Parqueadero parqueadero) {
        parqueadero.setEstado(true);
        parqu_dao.save(parqueadero);
    }

    @Override
    public Parqueadero encontrarparqueadero(Long id) {
        return parqu_dao.findById(id).orElse(null);
    }

    @Override
    public Vehiculo encontrarvehiculo(String placa) {
        return Vehi_dao.findByPlaca(placa);
    }

    @Override
    public Long contarvehiculospark() {
        return parqu_dao.count();
    }

    @Override
    public void eliminarparq(Long id) {
        parqu_dao.deleteById(id);
    }

}
