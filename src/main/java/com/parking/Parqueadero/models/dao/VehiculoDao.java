package com.parking.Parqueadero.models.dao;

import com.parking.Parqueadero.models.entity.Vehiculo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VehiculoDao extends JpaRepository<Vehiculo,Long> {
    Vehiculo findByPlaca(String plca);

}
