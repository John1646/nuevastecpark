package com.parking.Parqueadero.models.dao;

import com.parking.Parqueadero.models.entity.Parqueadero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParqueaderoDao extends JpaRepository<Parqueadero,Long> {
}
