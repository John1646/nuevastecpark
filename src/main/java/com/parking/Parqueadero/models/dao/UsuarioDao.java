package com.parking.Parqueadero.models.dao;

import com.parking.Parqueadero.models.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UsuarioDao extends JpaRepository<Usuario,Long> {
    public Usuario findByName(String name);
}
