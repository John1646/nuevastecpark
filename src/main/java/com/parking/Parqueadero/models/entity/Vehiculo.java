package com.parking.Parqueadero.models.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Vehiculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 6,nullable = false)
    private String placa;
    private String tipo;
@ManyToOne
private Usuario usuario;


}
