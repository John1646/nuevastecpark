package com.parking.Parqueadero.models.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Entity
@Data
public class Parqueadero {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long parqueadero_id;
    @Column(length = 6,nullable = false)
    private String placa;
    @Temporal(TemporalType.TIMESTAMP)
    private Date ingreso;
    private boolean estado;



    @PrePersist
    public void Prepersist(){
        ingreso = new Date();
    }

    public String salida(){
        Date salida = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechaSalida=formato.format(salida);
        return fechaSalida;
    }

    public long total(){
        Date salida = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechaSalida=formato.format(salida);
        long tiempoDiferencia=salida.getTime()-ingreso.getTime();
        TimeUnit unidadTiempo=TimeUnit.MINUTES;
        long tiempopark=
                unidadTiempo.convert(tiempoDiferencia,TimeUnit.MILLISECONDS);
        if (tiempopark < 1){
            long total = 150;
            return total;
        } else {
            Long total = tiempopark * 150;
            return total;
        }
    }
}
