package com.parking.Parqueadero.controllers;

import com.parking.Parqueadero.models.entity.Parqueadero;
import com.parking.Parqueadero.models.entity.Usuario;
import com.parking.Parqueadero.models.entity.Vehiculo;
import com.parking.Parqueadero.models.services.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    IUsuarioService serviceUser;
    @GetMapping("/vehiculos")
    public String index(Model model){
    model.addAttribute("vehiculos",serviceUser.calluser());
        return "admin";
    }
    @GetMapping("/")
    public String home (Model model){
        model.addAttribute("usuario",new Usuario());
        model.addAttribute("vehiculo",new Vehiculo());
        model.addAttribute("parqueadero", new Parqueadero());
        return "users/index";
    }
    @PostMapping("/vehiculos")
    public  String create (Usuario usuario, Vehiculo vehiculo, RedirectAttributes flash){
        serviceUser.guardarusuario(usuario,vehiculo);
        flash.addFlashAttribute("info", "Se agrego correctamente el cliente y su vehiculo");
        return "redirect:/vehiculos";
    }
    @PostMapping("/parqueaderos")
    public String createparque (Parqueadero parqueadero, RedirectAttributes flash){
        Vehiculo vehiculo = serviceUser.encontrarvehiculo(parqueadero.getPlaca());
        if(serviceUser.contarvehiculospark()<=20 && vehiculo!= null) {
            flash.addFlashAttribute("info", "Se agrego correctamente el vehiculo al parqueadero");
            serviceUser.saveparqueadero(parqueadero);
        }else if(vehiculo == null){
            flash.addFlashAttribute("error", "el vehiculo con placa "+parqueadero.getPlaca()+" no se encuentra registrado");
            return "redirect:/";
        }
        else {
            flash.addFlashAttribute("error", "El parqueadero ha llegado a su tope de ocupación");
            return "redirect:/";
        }
        return "redirect:/parqueaderos";
    }
    @GetMapping("/parqueadero/{id}/{placa}")
    public  String encontrarparqu (@PathVariable Long id, Model model,@PathVariable String placa){
        Parqueadero parqueadero = serviceUser.encontrarparqueadero(id);
        Vehiculo vehiculo = serviceUser.encontrarvehiculo(placa);
        model.addAttribute("vehiculo",vehiculo);
        model.addAttribute("parqueadero",parqueadero);
        return "users/detalle";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id, RedirectAttributes flash){
        serviceUser.eliminarparq(id);
        flash.addFlashAttribute("info", "El espacio "+id+ " ha sido desocupado del parqueadero");
        return "redirect:/";
    }
    @GetMapping("/parqueaderos")
    public String indexParqueadero(Model model){
        model.addAttribute("parqueaderos", serviceUser.findAllPark());
        return "users/listar_parqueadero";
    }

}
